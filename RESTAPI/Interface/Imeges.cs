﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RESTAPI.Models;

namespace RESTAPI.Interface
{
    public interface Imeges
    {
       IEnumerable<Image> Image { get; }
    }
}
