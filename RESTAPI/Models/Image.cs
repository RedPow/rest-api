﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RESTAPI.Models
{
    public class Image
    {
        public string URL { get; set; }

        public UInt32 Price { get; set; }//Этот элемент не обязателен (для примера)

    }
}
