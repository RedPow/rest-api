﻿using RESTAPI.Interface;
using RESTAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RESTAPI.Mocks
{
    public class MockImages : Imeges
    {
        IEnumerable<Image> Imeges.Image {
            get
            {
                return new List<Image>
                {
                    new Image
                    {
                        URL = "https://bipbap.ru/wp-content/uploads/2017/10/0_8eb56_842bba74_XL-640x400.jpg",
                        Price = 1000
                    },
                    new Image
                    {
                        URL = "http://www.radionetplus.ru/uploads/posts/2013-05/1369460621_panda-26.jpg",
                        Price = 100
                    }
                };
            }
        }
    }
}
