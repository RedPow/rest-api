﻿using RESTAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RESTAPI.ViewModels
{
    public class ImagesVeiwModel
    {
        public IEnumerable<Image> getAllImages { get; set; }
    }
}
