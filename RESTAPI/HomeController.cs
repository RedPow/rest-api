﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Drawing;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using RESTAPI.Interface;



// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RESTAPI
{
    public class HomeController : Controller
    {
        private readonly Imeges _allImeges;
        
        public HomeController(Imeges iallImages)
        {
            _allImeges = iallImages;
        }

        public ViewResult List()
        {
            var Images = _allImeges.Image;
            return View(Images);
        }
    }
}
